#include <FileIO.h>
#include <Wire.h>
#include <Bridge.h>
#include <Process.h>

#define soundPin 0 // pin number for the pin we have the sound sensor in
#define soundLedPin 8
#define window 50 // window for listening on the sound sensor (in milliseconds)

unsigned int soundVal = 0; // initialize a the value from the sensor
double volts, decibels;
String dateString, timeString, curlString;
Process dateProcess;

void setup() {
  Bridge.begin();
  Serial.begin(9600);
  FileSystem.begin();
  pinMode(soundPin, INPUT); // set sound detection pin to input
  pinMode(soundLedPin, OUTPUT); // set second led pin to output

  //prepare the dateTime process for initial cycle
  if(!dateProcess.running()){
    dateProcess.begin("date");
    dateProcess.addParameter("+ %m%d%y %H%M%S");
    dateProcess.run();
  }
}

void loop() {
  String output = "";
  unsigned int signalMax = 0; 
  unsigned int signalMin = 1024;
  unsigned long startMillis= millis(); 

  //this takes sound readings for 50 milliseconds
  //the output is determined by taking the difference of the max and min
  while (millis() - startMillis < window){
     soundVal = analogRead(soundPin);
     if (soundVal < 1024){
        if (soundVal > signalMax)
          signalMax = soundVal; 
        else if (soundVal < signalMin)
          signalMin = soundVal;
     }
  }

  //take reading from dateProcess and parse into date and time
  timeString = dateProcess.readString();
  timeString.trim();
  dateString = timeString.substring(0,timeString.indexOf(" "));
  timeString = timeString.substring(timeString.indexOf(" ") + 1);

  volts = (signalMax - signalMin) * 3.3 / 1024;  // convert to volts
  decibels = 20 * log10(volts/0.1) + 50;        // convert to decibels
  output = dateString + "|" + timeString + "|" + String(decibels); // create string to print to text file

  //if decibel level is dangerous, turn on LED pin
  //set to 50 now for testing purposes
  //will set to 85 for real conditions
  if(decibels > 60)
    digitalWrite(soundLedPin, HIGH);
  else
    digitalWrite(soundLedPin, LOW);
    
  //this portion of the code opens the file on the SD card and saves
  //the sensor output locally
  File outputFile = FileSystem.open("/mnt/sd/datalog.txt", FILE_APPEND); // open the text file
  if(outputFile){
    outputFile.println(output); // print to the text file
    outputFile.close(); // close the text file
  }

  //create process for curl command and send output
  curlString = "curl -k -X POST https://arduinosound.firebaseio.com/sensorvalue.json -d '{\"date\" : \"" + dateString + "\", \"time\" : \"" + timeString + "\", \"value\" : " + String(decibels) + "}'";
  Process curlProcess;
  curlProcess.runShellCommand(curlString); 
  while(curlProcess.running());

  //prepare the dateTime process for next cycle
  if(!dateProcess.running()){
    dateProcess.begin("date");
    dateProcess.addParameter("+ %m%d%y %H%M%S");
    dateProcess.run();
  }
}
